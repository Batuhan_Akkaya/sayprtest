import React, {Component} from 'react';
import {View, StatusBar} from 'react-native';
import sharedStyles from "./src/utils/sharedStyles";
import AppNavigator  from './src/config/appNavigator';

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <View style={sharedStyles.wrapper}>
                <StatusBar barStyle={'light-content'} />
                <AppNavigator />
            </View>
        );
    }
}