import {createStackNavigator, createAppContainer} from 'react-navigation';

import {Main, FriendList} from '../screens';
import vars from "./vars";

const appNavigator = createStackNavigator({
    Main: {screen: Main},
    FriendList: {screen: FriendList}
}, {
    defaultNavigationOptions: {
        headerStyle: {backgroundColor: vars.baseColor},
        headerTintColor: '#fff',
        headerTitleStyle: {fontFamily: vars.font, fontSize: 17}
    }
});

export default createAppContainer(appNavigator);