export default {
    baseColor: '#36394c',
    orange: '#f4a118',
    font: 'room-bold',
    fontThin: 'Room-Thin'
}