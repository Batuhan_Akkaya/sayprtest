import {Share, Linking} from 'react-native';
import {shareOnFacebook, shareOnTwitter} from 'react-native-social-share';

export function shareApp() {
    Share.share({
        message: 'Lorem ipsum dolor sit amet',
        url: 'http://saypr.com',
        title: 'Wow, share'
    }, {
        dialogTitle: 'Share app',
    })
}

export function facebookShare() {
    shareOnFacebook({
            'text':'Lorem ipsum dolor sit amet.',
            'link':'https://saypr.com/',
            'imagelink':'https://via.placeholder.com/300'
        },
        (results) => {
            if (results == 'not_available')
                alert('Facebook not installed')
        }
    );
}

export function tweet() {
    shareOnTwitter({
            'text':'Lorem ipsum dolor sit amet.',
            'link':'https://saypr.com/',
            'imagelink':'https://via.placeholder.com/300'
        },
        (results) => {
            if (results == 'not_available')
                alert('Twitter not installed')
        }
    );
}

export function whatsapp() {
    Linking.openURL('whatsapp://send?text=The text')
}