import vars from "../config/vars";

export default {
    wrapper: {
        flex: 1
    },
    txt: {
        fontFamily: vars.font,
        color: vars.baseColor
    },
    centerText: {
        textAlign: 'center'
    }
}