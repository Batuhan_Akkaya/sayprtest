import React, {Component} from 'react';
import {View, Modal, Text, ImageBackground, Image, Clipboard, FlatList} from 'react-native';
import sharedStyles from "../../utils/sharedStyles";
import styles from "../../screens/main/styles";
import {Button, CloseIcon, Header} from "../index";
import Communications from 'react-native-communications';
import {facebookShare, tweet, whatsapp} from "../../utils/helpers";

class InviteModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttons: [
                {text: 'EMAIL', action: () => Communications.email(['mail@batuhanakkaya.com'], [], [], 'Test', 'Lorem ipsum dolor')},
                {text: 'SMS', action: () => Communications.text("00000000", "Hello")},
                {text: 'COPY LINK', action: () => {Clipboard.setString('hello world'); alert('Link copied')}},
                {text: 'FACEBOOK', action: () => facebookShare()},
                {text: 'TWITTER', action: () => tweet()},
                {text: 'WHATSAPP', action: () => whatsapp()},
            ]
        }
    }
    render() {
        return (
            <Modal visible={this.props.visible} onRequestClose={() => this.props.close()} animationType={'slide'}>
                <View style={sharedStyles.wrapper}>
                    <Header
                        title={'INVITE FRIENDS'}
                        right={<CloseIcon onPress={() => this.props.close()} />}
                    />
                    <View style={sharedStyles.wrapper}>
                        <ImageBackground style={styles.head} source={require('../../assets/images/head-bg.jpg')} resizeMode={'stretch'}>
                            <Image source={require('../../assets/images/email-icon.png')} style={styles.emailIcon} />
                            <Text style={styles.title}>Invite your friends</Text>
                            <Text style={styles.title}>and share the fun!</Text>
                        </ImageBackground>
                        <View style={[styles.body, {padding: 0}]}>
                            <FlatList
                                data={this.state.buttons}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({item}) =>
                                    <Button title={item.text} onPress={() => item.action()} style={{alignItems: 'flex-start', backgroundColor: '#fff', borderBottomWidth: 1, borderColor: '#ddd', paddingLeft: 20}} textStyle={{color: '#ddd'}} />
                                }
                            />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

export default InviteModal;