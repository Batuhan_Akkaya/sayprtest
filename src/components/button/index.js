import React, {Component} from 'react';
import {TouchableOpacity, Text, Image} from 'react-native';
import styles from "./styles";

class Button extends Component {
    render() {
        return (
            <TouchableOpacity style={[styles.btn, this.props.style]} onPress={this.props.onPress}>
                {this.props.img ?
                    <Image source={this.props.img} style={this.props.imgStyle} />
                    :
                    <Text style={[styles.title, this.props.textStyle]}>{this.props.title}</Text>
                }
            </TouchableOpacity>
        )
    }
}

export default Button;