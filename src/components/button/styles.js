import vars from "../../config/vars";

export default {
    btn: {
        backgroundColor: '#1a8ff7',
        justifyContent: 'center',
        alignItems: 'center',
        height: 55
    },
    title: {
        color: '#fff',
        fontFamily: vars.font,
        fontSize: 18,
        textAlign: 'center'
    }
}