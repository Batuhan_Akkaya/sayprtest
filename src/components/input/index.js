import React, {Component} from 'react';
import {View, TextInput} from 'react-native';

class Input extends Component {
    render() {
        return (
            <TextInput
                value={this.props.value}
            />
        );
    }
}

export default Input;