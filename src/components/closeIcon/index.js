import React, {Component} from 'react';
import {TouchableOpacity, Image} from 'react-native';

class CloseIcon extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.props.onPress} style={{width: 35, height: 35, marginRight: 10, marginTop: 3}}>
                <Image source={require('../../assets/images/close-icon.png')} style={{width: 23, height: 23}} />
            </TouchableOpacity>
        )
    }
}

export default CloseIcon;