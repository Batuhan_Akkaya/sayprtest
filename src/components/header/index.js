import React, {Component} from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

class Header extends Component {
    render() {
        return (
            <View style={styles.header}>
                <View style={[styles.col, styles.left]}>
                    {this.props.left}
                </View>
                <View style={styles.col}>
                    <Text style={styles.title}>{this.props.title}</Text>
                </View>
                <View style={[styles.col, styles.right]}>
                    {this.props.right}
                </View>
            </View>
        )
    }
}

export default Header;