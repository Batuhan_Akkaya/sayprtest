import {Header} from 'react-navigation';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import vars from "../../config/vars";

export default {
    header: {
        height: Header.HEIGHT,
        backgroundColor: vars.baseColor,
        flexDirection: 'row',
        alignItems: 'center',
        ...ifIphoneX({
            height: Header.HEIGHT + 20,
            paddingTop: 30
        })
    },
    col: {
        flex: 1
    },
    title: {
        color: '#fff',
        fontSize: 17,
        fontFamily: vars.font,
        textAlign: 'center'
    },
    right: {
        alignItems: 'flex-end'
    }
}