import React, {Component} from 'react';
import {View, ImageBackground, Image, Text} from 'react-native';
import sharedStyles from "../../utils/sharedStyles";
import styles from "./styles";
import {Button, CloseIcon} from "../../components";
import {shareApp} from "../../utils/helpers";

class Main extends Component {
    static navigationOptions = {
        title: 'INVITE FRIENDS',
        headerRight: <CloseIcon/>
    };

    render() {
        return (
            <View style={sharedStyles.wrapper}>
                <ImageBackground style={styles.head} source={require('../../assets/images/head-bg.jpg')} resizeMode={'stretch'}>
                    <Image source={require('../../assets/images/email-icon.png')} style={styles.emailIcon} />
                    <Text style={styles.title}>Invite your friends</Text>
                    <Text style={styles.title}>and share the fun!</Text>
                </ImageBackground>
                <View style={styles.body}>
                    <Button title={'INVITE FRIENDS'} onPress={() => this.props.navigation.navigate('FriendList')} />
                    <View style={styles.row}>
                        <View style={styles.divider} />
                        <Text style={[sharedStyles.txt, styles.subText]}>OR</Text>
                        <View style={styles.divider} />
                    </View>
                    <Text style={[styles.text, sharedStyles.centerText]}>SHARE YOUR LINK</Text>
                    <View style={styles.row}>
                        <Button title={'MARITSA'} style={[styles.btn, {flex: 3, marginRight: 15}]} textStyle={styles.btnTxt} onPress={() => shareApp} />
                        <Button img={require('../../assets/images/share-icon.png')} style={[styles.btn, {flex: 1}]} imgStyle={{width: 25, height: 25}} onPress={() => shareApp()} />
                    </View>
                </View>
            </View>
        );
    }
}

export default Main;