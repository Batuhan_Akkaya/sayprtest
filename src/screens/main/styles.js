import vars from "../../config/vars";

export default {
    head: {
        flex: 2,
        backgroundColor: vars.orange,
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        flex: 3,
        backgroundColor: '#f6f6f6',
        justifyContent: 'center',
        padding: 25
    },
    emailIcon: {
        width: 110,
        height: 90,
        resizeMode: 'contain',
        marginBottom: 35,
    },
    title: {
        fontSize: 34,
        fontFamily: vars.font,
        color: '#fff',
        textAlign: 'center',
        marginHorizontal: 20,
        lineHeight: 25
    },
    text: {
        color: '#777',
        fontSize: 17,

    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 30
    },
    divider: {
        flex: 1,
        height: 1,
        backgroundColor: vars.baseColor
    },
    subText: {
        fontSize: 18,
        marginHorizontal: 15,
        fontFamily: vars.fontThin
    },
    btn: {
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: vars.baseColor
    },
    btnTxt: {
        fontSize: 20,
        color: vars.baseColor,
        letterSpacing: 2
    }
}