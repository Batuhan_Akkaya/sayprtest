import React, {Component} from 'react';
import {View, Text, Image, ActivityIndicator, TouchableOpacity} from 'react-native';
import sharedStyles from "../../utils/sharedStyles";
import Api from 'axios';
import styles from './styles';
import AlphaScrollFlatList from 'alpha-scroll-flat-list';
import vars from "../../config/vars";
import {InviteModal} from "../../components";

class FriendList extends Component {
    static navigationOptions = {
        title: 'INVITE FRIENDS'
    };

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true,
            modalVisible: false
        }
    }

    componentDidMount() {
        Api.get('https://jsonplaceholder.typicode.com/users')
            .then(res => {
                this.setState(
                    {
                        data: res.data,
                        loading: false
                    }
                )
            });
    }

    render() {
        return (
            <View style={[sharedStyles.wrapper, {paddingTop: 15}]}>
                {this.state.loading ?
                    <View style={styles.loading}>
                        <ActivityIndicator color={vars.baseColor} size={'large'} />
                    </View>
                    :
                    <AlphaScrollFlatList
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.data.sort((prev, next) => prev.name.localeCompare(next.name))}
                        renderItem={({item}) =>
                            <TouchableOpacity style={styles.listItem} onPress={() => this.setState({modalVisible: true})}>
                                <Image source={require('../../assets/images/img_avatar.png')} style={styles.avatar} />
                                <View>
                                    <Text style={styles.name}>{item.name}</Text>
                                    <Text style={styles.email}>{item.email}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                        scrollKey={'name'}
                        reverse={false}
                        itemHeight={50}
                    />
                }
                <InviteModal visible={this.state.modalVisible} close={() => this.setState({modalVisible: false})} />
            </View>
        )
    }
}

export default FriendList;