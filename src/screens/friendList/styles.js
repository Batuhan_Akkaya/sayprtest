import vars from "../../config/vars";

export default {
    listItem: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
        paddingLeft: 15,
        borderBottomColor: '#ddd',
        borderBottomWidth: 1
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 25,
        marginRight: 15
    },
    name: {
        fontSize: 15,
        color: vars.baseColor,
        fontFamily: vars.font
    },
    email: {
        fontSize: 15,
        color: '#777',
        fontFamily: vars.fontThin
    },
    loading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
}